package com.founder.controller;

import com.founder.core.log.MyLog;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
@RequestMapping("/notify")
public class PayNotifyController {

    private final static MyLog _log = MyLog.getLog(PayNotifyController.class);

    @RequestMapping("/payNotify")
    public void payNotify(HttpServletRequest request, HttpServletResponse response) {
        _log.info("接收到通知");
        outResult(response, "success");
    }

    void outResult(HttpServletResponse response, String content) {
        response.setContentType("text/html");
        PrintWriter pw;
        try {
            pw = response.getWriter();
            pw.print(content);
            _log.info("通知应答完毕.");
        } catch (IOException e) {
            _log.error(e, "通知应答异常.");
        }
    }
}
