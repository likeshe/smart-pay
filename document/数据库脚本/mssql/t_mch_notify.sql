
CREATE TABLE [dbo].[t_mch_notify](
	[OrderId] [varchar](30) NOT NULL,
	[MchId] [varchar](30) NOT NULL,
	[MchOrderNo] [varchar](30) NOT NULL,
	[OrderType] [varchar](8) NOT NULL,
	[NotifyUrl] [varchar](2048) NOT NULL,
	[NotifyCount] [int] NOT NULL,
	[Result] [varchar](2048) NULL,
	[Status] [int] NOT NULL,
	[LastNotifyTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK__t_mch_no__C3905BCF5CD6CB2B] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Notif__5EBF139D]  DEFAULT ((0)) FOR [NotifyCount]
GO

ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Resul__5FB337D6]  DEFAULT (NULL) FOR [Result]
GO

ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Statu__60A75C0F]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__LastN__619B8048]  DEFAULT (NULL) FOR [LastNotifyTime]
GO

ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Creat__628FA481]  DEFAULT (getdate()) FOR [CreateTime]
GO

ALTER TABLE [dbo].[t_mch_notify] ADD  CONSTRAINT [DF__t_mch_not__Updat__6383C8BA]  DEFAULT (getdate()) FOR [UpdateTime]
GO


