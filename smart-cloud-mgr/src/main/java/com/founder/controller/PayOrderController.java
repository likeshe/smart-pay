package com.founder.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.founder.core.domain.PayOrder;
import com.founder.core.log.MyLog;
import com.founder.core.page.PageModel;
import com.founder.service.IPayOrderService;
import com.founder.core.utils.AmountUtil;
import com.founder.core.utils.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/pay_order")
public class PayOrderController {

    private final static MyLog _log = MyLog.getLog(PayOrderController.class);

    @Autowired
    private IPayOrderService payOrderService;

    @RequestMapping("/list.html")
    public String listInput(ModelMap model) {
        return "pay_order/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute PayOrder payOrder, Integer pageIndex, Integer pageSize) {
        _log.info("分页查询订单列表，参数pageIndex={}，pageSize={}。", pageIndex, pageSize);
        PageModel pageModel = new PageModel();
        Page<PayOrder> page = payOrderService.selectPayOrderList((pageIndex-1)*1, pageSize, payOrder);
        int count = Long.valueOf(page.getTotalElements()).intValue();
        if(count <= 0) return JSON.toJSONString(pageModel);
        List<PayOrder> payOrderList = page.getContent();
        if(!CollectionUtils.isEmpty(payOrderList)) {
            JSONArray array = new JSONArray();
            for(PayOrder po : payOrderList) {
                JSONObject object = (JSONObject) JSONObject.toJSON(po);
                if(po.getCreateTime() != null) object.put("createTime", DateUtil.date2Str(po.getCreateTime()));
                if(po.getAmount() != null) object.put("amount", AmountUtil.convertCent2Dollar(po.getAmount()+""));
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/view.html")
    public String viewInput(String payOrderId, ModelMap model) {
        PayOrder item = null;
        if(StringUtils.isNotBlank(payOrderId)) {
            item = payOrderService.selectPayOrder(payOrderId);
        }
        if(item == null) {
            item = new PayOrder();
            model.put("item", item);
            return "pay_order/view";
        }
        JSONObject object = (JSONObject) JSON.toJSON(item);
        if(item.getPaySuccTime() != null) object.put("paySuccTime", DateUtil.date2Str(new Date(item.getPaySuccTime())));
        if(item.getLastNotifyTime() != null) object.put("lastNotifyTime", DateUtil.date2Str(new Date(item.getLastNotifyTime())));
        if(item.getExpireTime() != null) object.put("expireTime", DateUtil.date2Str(new Date(item.getExpireTime())));
        if(item.getAmount() != null) object.put("amount", AmountUtil.convertCent2Dollar(item.getAmount()+""));
        model.put("item", object);
        return "pay_order/view";
    }
}
